<?php
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\Models\Invoice::class, function (Faker $faker) {
    $discount = $faker->numberBetween(100, 500);
    $sub_total = $faker->numberBetween(1000, 5000);
    return [
        'customer_id' => mt_rand(1, 50),
        'title'       => $faker->sentence,
        'date'        => '2019-'.mt_rand(1, 6).'-'.mt_rand(1,28),
        'due_date'    => '2019-'.mt_rand(7, 12).'-'.mt_rand(1,28),
        'discount'    => $discount,
        'sub_total'   => $sub_total,
        'total'       => $sub_total - $discount
    ];
});