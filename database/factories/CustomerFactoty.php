<?php
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\Models\Customer::class, function (Faker $faker) {
    return [
        'name'    => $faker->name,
        'company' => $faker->company,
        'email'   => $faker->unique()->email,
        'phone'   => $faker->phoneNumber,
        'address' => $faker->address
    ];
});