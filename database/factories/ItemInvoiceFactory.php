<?php
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\Models\ItemInvoice::class, function (Faker $faker) {
    return [
        'invoice_id' => function() {
            return factory(App\Models\Invoice::class)->create()->id;
        },
        'description' => $faker->sentence,
        'qty'         => mt_rand(1, 10),
        'unit_price'  => $faker->numberBetween(500, 4000)

    ];
});