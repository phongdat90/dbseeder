<?php

use Illuminate\Database\Seeder;

class ItemInvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(App\Models\ItemInvoice::class, 150)->create();
    }
}
