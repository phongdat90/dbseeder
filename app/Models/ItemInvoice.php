<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemInvoice extends Model
{
    protected $table = 'item_invoices';
}
